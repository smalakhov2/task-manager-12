package ru.malakhov.tm.api.controller;

public interface ITaskController {

    void displayTasks();

    void clearTasks();

    void createTask();

    void displayTaskById();

    void displayTaskByIndex();

    void displayTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

}
